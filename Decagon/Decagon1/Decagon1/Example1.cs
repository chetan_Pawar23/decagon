﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decagon1
{
    class Example1
    {
        static void Main(string[] args)
        {
                                   
            Console.WriteLine("Enter the two digit Number.");

            int num1,num3,num4,sum = 0;
            
            num1 = int.Parse(Console.ReadLine());
            if(num1.ToString().Count() != 2)
            {
                Console.WriteLine("Error! Number is not two digits. Please enter a two digit number Only");
               
             }            
                else if (num1 != 0)
                {               
                sum = SumDigit(num1);
                    num3= num1 - sum;
                Console.WriteLine("Sum the digits of the number : " + sum);
                Console.WriteLine("Subtract the sum from the two digit : " + (num3));               
                num4 = SumDigit(num3);
                Console.WriteLine("sum of the digits obtained as a result of:"+(num4));
            }

            
            Console.ReadLine();
        }
        public static int SumDigit(int number)
        {
            int resultsum;
            int r = number % 10;
            int num2 = number / 10;
            resultsum = num2 + r;
            return resultsum; 
       }
    }
}
