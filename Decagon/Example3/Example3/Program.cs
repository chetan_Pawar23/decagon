﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Example3
{
    class Program
    {

        public String ConvertToUpperCase(String input)
        {
            String output = "";
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] >= 'a' && input[i] <= 'z')
                {
                    output += (char)(input[i] - 'a' + 'A');
                }
                else
                    output += input[i];
            }
            return output;
        }
        public String ConvertToLowerCase(String input)
        {

           
            String output = "";
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] >= 'A' && input[i] <= 'Z')
                {
                    output += (char)(input[i] - 'A' + 'a');
                }
                else
                    output += input[i];
            }
            return output;
        }


        public string addSpaceAfterLetter(string input)
        {
            // input = "sfdjhjlk";

           
            StringBuilder bar = new StringBuilder();
       
            foreach (char c in input)
            {
                bar.Append(c+" ");
                
            
             }
            return ""+bar;
        }            
        public string RemoveWhitespace(string input)
        {
            int j = 0, inputlen = input.Length;
            char[] newarr = new char[inputlen];

            for (int i = 0; i < inputlen; ++i)
            {
                char tmp = input[i];

                if (!char.IsWhiteSpace(tmp))
                {
                    newarr[j] = tmp;
                    ++j;
                }
            }
            return new string(newarr, 0, j);
        }
        static void Main(string[] args) {


            Program program = new Program();
           Console.Write("Enter a string: ");
           String input = Console.ReadLine();
            input = program.ConvertToUpperCase(input);            
               System.Console.WriteLine("\nConverted String in Upper Case: " + input);
           
            string output = program.addSpaceAfterLetter(input);
            Console.WriteLine("\n Add Spaces after each letter: "+ output);

           String removespace = program.RemoveWhitespace(input);

            Console.WriteLine("\n string remove_space:"+removespace);
            Console.ReadLine();

        }
    }
}
