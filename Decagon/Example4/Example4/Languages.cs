﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example4
{
    class Languages
    {
        public static void LanguageMap(Dictionary<string, string> language)

        {

            language.Add("Afrikaans", "af");

            language.Add("Albanian", "sq");

            language.Add("Arabic", "ar");

            language.Add("Armenian", "hy");

            language.Add("Azerbaijani", "az");

            language.Add("Basque", "eu");

            language.Add("Belarusian", "be");

            language.Add("Bengali", "bn");

            language.Add("Bulgarian", "bg");

            language.Add("Catalan", "ca");

            language.Add("Chinese", "zh-CN");

            language.Add("Croatian", "hr");

            language.Add("Czech", "cs");

            language.Add("Danish", "da");

            language.Add("Dutch", "nl");

            language.Add("English", "en");

        }

    }

}

