﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Example4;
using System.Net;
using System.Web;

namespace Example4
{
    class Program
    {
       // private static readonly object HttpUtility;

        static void Main(string[] args)
        {
            try

            {

                string fromLanguage, toLanguage, text;

                text = "hello";

                fromLanguage = "English";

                toLanguage = "Danish";

                var language = new Dictionary<string, string>();

                Languages.LanguageMap(language);

                Console.WriteLine("Your Text: " + text);

                Uri address = new Uri("http://translate.google.com/translate_t");

                System.Net.WebClient webClient = new WebClient();

                webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                webClient.UploadStringCompleted += new UploadStringCompletedEventHandler(webClient_UploadStringCompleted);

                webClient.UploadStringAsync(address, GetPostData(language[fromLanguage], language[toLanguage], text));

                Console.ReadKey();

            }

            catch (Exception ex)

            {

                Console.WriteLine(ex);

                Console.ReadKey();

            }

        }

        static string GetPostData(string fromLanguage, string toLanguage, string text)

        {

            string data = string.Format("hl=en&ie=UTF8&oe=UTF8submit=Translate&langpair={0}|{1}", fromLanguage, toLanguage);

            //object HttpUtility = null;
            return data += "&text=" + HttpUtility.UrlEncode(text);

        }

        static void webClient_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)

        {

            if (e.Result != null)

            {

                var document = new HtmlDocument(); // Here creating new html document to store my page html

                document.LoadHtml(e.Result); // now load getting html

                var node = document.DocumentNode.SelectSingleNode("//span[@id='result_box']"); // finding particular tag to get value from html document

                var output = node != null ? node.InnerText : e.Error.Message;

                Console.WriteLine("Translated Text: " + output); // show the output

            }

        }

    }

}
    

