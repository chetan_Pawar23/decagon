﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Example2
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.Write("Enter you string: ");
            string str = Console.ReadLine();
             string resultStr = Regex.Replace(str, "[aeiouAEIOU]", ",");
             Console.WriteLine(resultStr);
             Console.ReadLine();
        }
    }
}
